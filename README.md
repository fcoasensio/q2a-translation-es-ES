# Question2Answer es-ES Spanish (Spain) translation

## Installation

1. Clone or download this repository
1. Copy the directory `es-ES` into `qa_lang` directory
1. Select the `Spanish (Spain)` translation from the **Admin** section

## Contribute

If you want to contribute, please read https://about.gitlab.com/contributing.

## Issues

Please, report issues via GitLab bugtracker (https://gitlab.com/midget/q2a-translation-es-ES/issues)

## License
This software is published under GPL2 License. See LICENSE file for more info.

